# python-selenium-behave
A BDD based Test framework using Python, Selenium Webdriver, Page Object Model and Behave

## reporting

$ behave -f allure_behave.formatter:AllureFormatter -o allure_results tests/features/
$ allure serve allure_results/
$ ./allure-commandline-2.10.0/allure-2.10.0/bin/allure serve allure_results/


## critical flows

1. Search for an item.                                                   [+]
2. Sort the results by price (lowest).                                   [+]
3. Add the item to the shopping cart.                                    [+]
4. Go to the shopping cart and proceed with the purchase.                [+]
5. The site will ask to create an account, so, complete this flow.       []
6. Make the purchase and confirm all the information.                    []
