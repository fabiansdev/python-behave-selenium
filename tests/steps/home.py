from behave import given, when, then
from pages.home import home

@then(u'I see component: "{component}"')
def step_impl_verify_component(context, component):
    home.verify_element(component)

@then(u'I see results in product container')
def step_impl_check_results_container(context):
    home.check_results_container()

@when(u'I search "{key_word}" in search bar')
def step_impl_text_search_bar(context, key_word):
    home.text_search_bar(key_word)

@when(u'I sort by "{sort_by}" search results')
def step_impl_sort_results_by(context, sort_by):
    home.sort_results_by(sort_by)

@when(u'I select first product in search results')
def step_impl_select_first_prod(context):
    home.select_first_prod()

@when(u'I proceed to checkout')
def step_impl_select_checkout_btn(context):
    home.select_checkout_btn()
