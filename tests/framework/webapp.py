from selenium import webdriver
from data.config import settings
from urllib.parse import urljoin
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager


class WebApp:
    instance = None

    @classmethod
    def get_instance(cls):
        if cls.instance is None:
            cls.instance = WebApp()
        return cls.instance

    def __init__(self):
        if str(settings['browser']).lower() is "firefox":
            self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        elif str(settings['browser']).lower() is "chrome":
            self.driver= webdriver.Chrome(executable_path=ChromeDriverManager().install())
        else:
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())

    def get_driver(self):
        return self.driver

    def load_website(self):
        self.driver.get(settings['url'])

    def goto_page(self, page):
        if page == 'home':
            return
        self.driver.get(urljoin(settings['url'], page.lower()))

    def verify_component_exists(self, component):
        assert component in self.driver.find_element_by_tag_name('body').text, \
            "Component {} not found on page".format(component)

    def get_locator(self, element):
        if 'id' in element.keys():
            return self.driver.find_element_by_id(element['id'])
        elif 'class' in element.keys():
            return self.driver.find_element_by_class_name(element['class'])
        elif 'name' in element.keys():
            return self.driver.find_element_by_name(element['name'])
        elif 'css' in element.keys():
            return self.driver.find_element_by_css_selector(element['css'])
        return None


webapp = WebApp.get_instance()
