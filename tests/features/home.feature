Feature: Home

Scenario Outline: Check for components
    Given I load the website
    When I go to "home" page
    Then I see component: "<rows>"
    Examples:
        | rows              |
        | Logo              |
        | Slider            |
        | Banner one        |
        | Banner two        |
        | Tab popular       |
        | Tab best sellers  |
        | Product list grid |

Scenario Outline: Search for a known product
    Given I load the website
    When I search "<key_word>" in search bar
    Then I see results in product container
    Examples:
        | key_word       |
        | dress          |

Scenario Outline: Search for a product and sort by lowest price
    Given I load the website
    When I search "<key_word>" in search bar
    When I sort by "<sort_by>" search results
    Examples:
        | key_word       | sort_by       |
        | dress          | lowest price  |

Scenario Outline: Search for a product and add first result to cart
    Given I load the website
    When I search "<key_word>" in search bar
    When I select first product in search results
    Examples:
        | key_word       |
        | dress          |

Scenario Outline: Add product to cart and complete purchase
    Given I load the website
    When I search "<key_word>" in search bar
    When I select first product in search results
    When I proceed to checkout
    Examples:
        | key_word       |
        | dress          |
