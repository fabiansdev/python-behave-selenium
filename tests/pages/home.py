from framework.webapp import webapp
from pages.mapping import locators
from selenium.webdriver.support.ui import Select


class Home():
    instance = None

    @classmethod
    def get_instance(cls):
        if cls.instance is None:
            cls.instance = Home()
        return cls.instance

    def __init__(self):
        self.driver = webapp.get_driver()

    def verify_element(self, row):
        status = None
        loc = locators[row]
        if 'id' in loc.keys():
            status = self.driver.find_element_by_id(loc['id']).text
        elif 'class' in loc.keys():
            status = self.driver.find_element_by_class_name(loc['class']).text
        assert status is not None

    def text_search_bar(self, key_word):
        elem = None
        bar = locators['Search bar']
        btn = locators['Search button']
        if 'id' in bar.keys():
            elem = self.driver.find_element_by_id(bar['id'])
        elif 'class' in loc.keys():
            elem = self.driver.find_element_by_class_name(bar['class'])
        elem2 = self.driver.find_element_by_name(btn['name'])
        elem.clear()
        elem.send_keys(key_word)
        elem2.click()

    def check_results_container(self):
        elem = None
        res = locators['Search result container']
        if 'id' in res.keys():
            elem = self.driver.find_element_by_id(res['id'])
        elif 'class' in res.keys():
            elem = self.driver.find_element_by_class_name(res['class'])
        assert elem is not None

    def sort_results_by(self, sort_by):
        elem = None
        prod_sort = locators['Product sorter']
        elem = Select(webapp.get_locator(prod_sort))
        if sort_by == 'lowest price':
            elem.select_by_visible_text('Price: Lowest first')

    def select_first_prod(self):
        first_prod = locators['First product']
        add_cart_btn = locators['Add cart button']
        first_prod = webapp.get_locator(first_prod)
        first_prod.click()
        add_cart_btn = webapp.get_locator(add_cart_btn)
        add_cart_btn.click()

    def select_checkout_btn(self):
        chk_btn = locators['Checkout button']
        self.driver.implicitly_wait(1)
        chk_btn = webapp.get_locator(chk_btn)
        chk_btn.click()


home = Home.get_instance()
