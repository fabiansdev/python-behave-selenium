#mapping.py
locators = {
    'Logo':                    {'id': 'header_logo'},
    'Slider':                  {'id': 'homeslider'},
    'Banner one':              {'id': 'htmlcontent_top'},
    'Banner two':              {'id': 'htmlcontent_top'},
    'Tab popular':             {'class': 'homefeatured'},
    'Tab best sellers':        {'class': 'blockbestsellers'},
    'Product list grid':       {'class': 'product_list grid row homefeatured tab-pane active'},
    'Search bar':              {'id': 'search_query_top'},
    'Search button':           {'name': 'submit_search'},
    'Search result container': {'class': 'product-image-container'},
    'Product sorter':          {'id': 'selectProductSort'},
    'First product':           {'class': 'product_img_link'},
    'Add cart button':         {'name': 'Submit'},
    'Checkout button':         {'css': 'a.button[title="Proceed to checkout"]'}
}
